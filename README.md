# AWS in Action: Lambda

==WARNING: This example assumes that you have installed and configured the [AWS Command Line Interface](https://aws.amazon.com/cli/)==

Clone this repository ...

```
$ git clone git@github.com:AWSinAction/lambda.git
$ cd lambda/
```

Create an S3 bucket for your Lambda code and upload the `lambda.zip` file (replace `$LambdaS3Bucket` with a S3 bucket name e.g. `lambda-michael`).

==WARNING: This bucket has nothing to do with the resizing of images. It contains the zipped source code of the Lambda function.==

```
export AWS_DEFAULT_REGION=us-west-2
$ aws s3 mb s3://$LambdaS3Bucket
$ aws s3 cp lambda.zip s3://$LambdaS3Bucket/lambda.zip
```

Create a CloudFormation stack (replace `$ImageS3Bucket` with a name for your image bucket e. g. `image-michael`, replace `$LambdaS3Bucket` with your Lambda code S3 bucket name, and replace $ACCUploader with the ARN for a user with sufficient permissions).

```
$ aws cloudformation create-stack --stack-name lambda-resize --template-body file://template.json --capabilities CAPABILITY_IAM --parameters ParameterKey=ImageS3Bucket,ParameterValue=$ImageS3Bucket ParameterKey=LambdaS3Bucket,ParameterValue=$LambdaS3Bucket ParameterKey=ACCUploader,ParameterValue=$ACCUploader
```

Wait until the stack is created (retry if you get `CREATE_IN_PROGRESS` this can take a few minutes). (I find it easier to monitor using the AWS CloudFormation Console)

```
$ aws cloudformation describe-stacks --stack-name lambda-resize --query Stacks[].StackStatus
[
    "CREATE_COMPLETE"
]
```

You can now upload your first image to the image S3 bucket (you can also use the web based [Management Console](https://console.aws.amazon.com/s3) if you prefer).

```
$ aws s3 cp path/to/image.png s3://$ImageS3Bucket/development/upload/image.png
```

You will see the resized images in the `resized` folder (you can also use the web based Management Console if you prefer).

---
This is where it stops working

---


```
$ aws s3 ls s3://$ImageS3Bucket/development/resized/
  PRE 150x/
  PRE 50x50/
  PRE x150/
```

As you can see, for every size configuration a new "directory" was created.

## What's next?

From this point you can think about how to enable your users to upload files into the `-original` S3 bucket if your use cases requires user generated content. To allow uploads from your users you should think about a way to protect your `-original` bucket with IAM permissions. You could use the [Security Token Service](http://docs.aws.amazon.com/STS/latest/APIReference/Welcome.html) to achieve this.

You should use CloudFront in combination with the `-resized` bucket to serve the resized images to your end users. This will decrease the latency by pushing your content to one of the edge locations of the CloudFront CDN network.

## Teardown

Remove all files in the image bucket.

```
$ aws s3 rm --recursive s3://$ImageS3Bucket
```

Delete the CloudFormation stack.

```
$ aws cloudformation delete-stack --stack-name lambda-resize
```

Delete Lambda code S3 bucket (replace `$LambdaS3Bucket`).

```
$ aws s3 rb --force s3://$LambdaS3Bucket
```

## Customize the code

If you want to make changes to the code you need to create a new lambda code file (`lambda.zip`) and upload the new zip to S3. You can adjust the `config.json` file to adjust the size configurations.

```
$ npm install
$ ./bundle.sh
$ aws s3 cp lambda.zip s3://$LambdaS3Bucket/lambda.zip
```

## Summary

AWS Lambda can respond to S3 events like a new file was uploaded. The Lambda function will download the original image from S3 to create new resized images. The resized images are then upload to S3 again. The Lambda solution in scalable and does not require any operational work.